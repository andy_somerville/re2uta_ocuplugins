/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <re2uta_ocuPlugins/NNPlugin.h>

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <std_msgs/Float64MultiArray.h>

NNPlugin::NNPlugin()
{
    // Create the layouts
    QHBoxLayout *a1hbox = new QHBoxLayout();
    QHBoxLayout *a2hbox = new QHBoxLayout();
    QHBoxLayout *a3hbox = new QHBoxLayout();
    QHBoxLayout *a4hbox = new QHBoxLayout();
    QHBoxLayout *a5hbox = new QHBoxLayout();
    QHBoxLayout *a6hbox = new QHBoxLayout();
    QHBoxLayout *a7hbox = new QHBoxLayout();
    QHBoxLayout *a8hbox = new QHBoxLayout();
    QVBoxLayout *vbox   = new QVBoxLayout(this);

    // Create the labels
    m_a1Label = new QLabel("a1");
    m_a2Label = new QLabel("a2");
    m_a3Label = new QLabel("a3");
    m_a4Label = new QLabel("a4");
    m_a5Label = new QLabel("a5");
    m_a6Label = new QLabel("a6");
    m_a7Label = new QLabel("a7");
    m_a8Label = new QLabel("a8");

    // Create and setup the spin boxes
    m_a1Term = new QSpinBox();
    m_a2Term = new QSpinBox();
    m_a3Term = new QSpinBox();
    m_a4Term = new QDoubleSpinBox();
    m_a5Term = new QDoubleSpinBox();
    m_a6Term = new QDoubleSpinBox();
    m_a7Term = new QDoubleSpinBox();
    m_a8Term = new QDoubleSpinBox();

//    m_a1Term->setSingleStep(0.01);
//    m_a2Term->setSingleStep(0.01);
//    m_a3Term->setSingleStep(0.01);
    m_a4Term->setSingleStep(0.01);
    m_a5Term->setSingleStep(0.01);
    m_a6Term->setSingleStep(0.01);
    m_a7Term->setSingleStep(0.01);
    m_a8Term->setSingleStep(0.01);

    // Add the labels to the layouts
    a1hbox->addWidget(m_a1Label);
    a2hbox->addWidget(m_a2Label);
    a3hbox->addWidget(m_a3Label);
    a4hbox->addWidget(m_a4Label);
    a5hbox->addWidget(m_a5Label);
    a6hbox->addWidget(m_a6Label);
    a7hbox->addWidget(m_a7Label);
    a8hbox->addWidget(m_a8Label);

    // Add the spin boxes to the layouts
    a1hbox->addWidget(m_a1Term);
    a2hbox->addWidget(m_a2Term);
    a3hbox->addWidget(m_a3Term);
    a4hbox->addWidget(m_a4Term);
    a5hbox->addWidget(m_a5Term);
    a6hbox->addWidget(m_a6Term);
    a7hbox->addWidget(m_a7Term);
    a8hbox->addWidget(m_a8Term);

    // Add the horizontal layouts to the vertical
    vbox->addLayout(a1hbox);
    vbox->addLayout(a2hbox);
    vbox->addLayout(a3hbox);
    vbox->addLayout(a4hbox);
    vbox->addLayout(a5hbox);
    vbox->addLayout(a6hbox);
    vbox->addLayout(a7hbox);
    vbox->addLayout(a8hbox);

    m_timer = m_node.createTimer(ros::Duration( 0.05), &NNPlugin::update, this);

    // Publishers
    m_nnTermPub = m_node.advertise< std_msgs::Float64MultiArray >( "/nn/a_terms", 1, this);

    show();
}

void NNPlugin::update( const ros::TimerEvent& e)
{
    std_msgs::Float64MultiArray msg;
    msg.data.push_back(((double)m_a1Term->value()));
    msg.data.push_back(((double)m_a2Term->value()));
    msg.data.push_back(((double)m_a3Term->value()));
    msg.data.push_back(         m_a4Term->value());
    msg.data.push_back(         m_a5Term->value());
    msg.data.push_back(         m_a6Term->value());
    msg.data.push_back(         m_a7Term->value());
    msg.data.push_back(         m_a8Term->value());

    m_nnTermPub.publish(msg);
}

/**
 * Required for rviz plug-in
 */
void NNPlugin::saveToConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config )
{
    config->set("nn_a1",                   m_a1Term->value());
    config->set("nn_a2",                   m_a2Term->value());
    config->set("nn_a3",                   m_a3Term->value());
    config->set("nn_a4",static_cast<float>(m_a4Term->value()));
    config->set("nn_a5",static_cast<float>(m_a5Term->value()));
    config->set("nn_a6",static_cast<float>(m_a6Term->value()));
    config->set("nn_a7",static_cast<float>(m_a7Term->value()));
    config->set("nn_a8",static_cast<float>(m_a8Term->value()));
}

/**
 * Required for rviz plug-in
 */
void NNPlugin::loadFromConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config )
{
    int   iVal;
    float fVal;

    config->get("nn_a1", &iVal, 0);
    m_a1Term->setValue(iVal);

    config->get("nn_a2", &iVal, 0);
    m_a2Term->setValue(iVal);

    config->get("nn_a3", &iVal, 0);
    m_a3Term->setValue(iVal);

    config->get("nn_a4", &fVal, 0.0);
    m_a4Term->setValue(static_cast<double>(fVal));

    config->get("nn_a5", &fVal, 0.0);
    m_a5Term->setValue(static_cast<double>(fVal));

    config->get("nn_a6", &fVal, 0.0);
    m_a6Term->setValue(static_cast<double>(fVal));

    config->get("nn_a7", &fVal, 0.0);
    m_a7Term->setValue(static_cast<double>(fVal));

    config->get("nn_a8", &fVal, 0.0);
    m_a8Term->setValue(static_cast<double>(fVal));
}

/**
 * Required for rviz plug-in
 */
void NNPlugin::onInitialize()
{
}

// Tell pluginlib about this class.  Every class which should be
// loadable by pluginlib::ClassLoader must have these two lines
// compiled in its .cpp file, outside of any namespace scope.
#include <pluginlib/class_list_macros.h>
PLUGINLIB_DECLARE_CLASS( re2uta_ocuPlugins, NN, NNPlugin, rviz::Panel )

