/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <re2uta_ocuPlugins/WalkPathPlugin.h>

#include <QHBoxLayout>
#include <QVBoxLayout>

#include <re2uta_inetComms/WalkingInfo.h>

static const std::string WC_BDI_STR("Bdi");
static const std::string WC_RE2UTA_STR("Re2Uta");
static const std::string WC_OFF_STR("off");
static const std::string WC_BAL_STR("Balance");
static const std::string WC_CAPTPT_STR("CapturePoint");
static const std::string WC_CRAWL_STR("Crawl");
static const std::string SG_CIRCLE_STR("Circle");
static const std::string SG_NAO_STR("Nao");
static const std::string SG_MANUAL_STR("Manual");
static const std::string JC_NEURALNETWORK_STR("NeuralNetwork");
static const std::string JC_PID_STR("Pid");
static const std::string FT_LEFT_STR("Left");
static const std::string FT_RIGHT_STR("Right");
static const std::string FT_FORWARD_STR("Forward");
static const std::string FT_BACKWARD_STR("Backward");

WalkPathPlugin::WalkPathPlugin()
{
    QHBoxLayout *topHbox = new QHBoxLayout();
    QHBoxLayout *bottomHbox = new QHBoxLayout();
    QVBoxLayout *vbox = new QVBoxLayout(this);

    m_queuePathButton = new QPushButton("Queue Path", this);
    m_sendPathButton  = new QPushButton("Send Path",  this);

    m_walkCommanderCBox   = new QComboBox();
    m_stepGenCBox         = new QComboBox();
    m_jointControllerCBox = new QComboBox();
    m_footCBox            = new QComboBox();

    m_walkCommanderCBox->addItem(tr(WC_BDI_STR.c_str()));
    m_walkCommanderCBox->addItem(tr(WC_RE2UTA_STR.c_str()));
    m_walkCommanderCBox->addItem(tr(WC_OFF_STR.c_str()));
    m_walkCommanderCBox->addItem(tr(WC_BAL_STR.c_str()));
    m_walkCommanderCBox->addItem(tr(WC_CAPTPT_STR.c_str()));
    m_walkCommanderCBox->addItem(tr(WC_CRAWL_STR.c_str()));
    m_stepGenCBox->addItem(tr(SG_CIRCLE_STR.c_str()));
    m_stepGenCBox->addItem(tr(SG_NAO_STR.c_str()));
    m_stepGenCBox->addItem(tr(SG_MANUAL_STR.c_str()));
    m_jointControllerCBox->addItem(tr(JC_NEURALNETWORK_STR.c_str()));
    m_jointControllerCBox->addItem(tr(JC_PID_STR.c_str()));
    m_footCBox->addItem(tr(FT_LEFT_STR.c_str()));
    m_footCBox->addItem(tr(FT_RIGHT_STR.c_str()));
    m_footCBox->addItem(tr(FT_FORWARD_STR.c_str()));
    m_footCBox->addItem(tr(FT_BACKWARD_STR.c_str()));

    topHbox->addWidget(m_walkCommanderCBox);
    topHbox->addWidget(m_stepGenCBox);
    topHbox->addWidget(m_jointControllerCBox);
    bottomHbox->addWidget(m_footCBox);
    bottomHbox->addWidget(m_queuePathButton);
    bottomHbox->addWidget(m_sendPathButton);

    vbox->addLayout(topHbox);
    vbox->addLayout(bottomHbox);

    setLayout(vbox);

    connect(m_queuePathButton,     SIGNAL(released()),     this, SLOT(queuePath()) );
    connect(m_sendPathButton,      SIGNAL(released()),     this, SLOT(sendPath())  );
    connect(m_walkCommanderCBox,   SIGNAL(activated(int)), this, SLOT(changeSettings()));
    connect(m_stepGenCBox,         SIGNAL(activated(int)), this, SLOT(changeSettings()));
    connect(m_jointControllerCBox, SIGNAL(activated(int)), this, SLOT(changeSettings()));
    connect(m_footCBox,            SIGNAL(activated(int)), this, SLOT(changeSettings()));

    m_walkPathPub = m_node.advertise< re2uta_inetComms::WalkingInfo >( "/ui/walkPathQueue", 1, this);

    show();
}

void WalkPathPlugin::queuePath()
{
    re2uta_inetComms::WalkingInfo msg;

    msg.changeSettingsOnly = false;
    msg.queueMsg = true;
    msg.walkCommander = m_walkCommanderCBox->currentText().toStdString();
    msg.stepGenerator = m_stepGenCBox->currentText().toStdString();
    msg.jointController = m_jointControllerCBox->currentText().toStdString();
    msg.footChoice = m_footCBox->currentText().toStdString();

    m_walkPathPub.publish(msg);
}

void WalkPathPlugin::sendPath()
{
    re2uta_inetComms::WalkingInfo msg;

    msg.changeSettingsOnly = false;
    msg.queueMsg = false;
    msg.walkCommander = m_walkCommanderCBox->currentText().toStdString();
    msg.stepGenerator = m_stepGenCBox->currentText().toStdString();
    msg.jointController = m_jointControllerCBox->currentText().toStdString();
    msg.footChoice = m_footCBox->currentText().toStdString();

    m_walkPathPub.publish(msg);
}

void WalkPathPlugin::changeSettings()
{
    re2uta_inetComms::WalkingInfo msg;

    msg.changeSettingsOnly = true;
    msg.queueMsg           = false;
    msg.walkCommander      = m_walkCommanderCBox->currentText().toStdString();
    msg.stepGenerator      = m_stepGenCBox->currentText().toStdString();
    msg.jointController    = m_jointControllerCBox->currentText().toStdString();
    msg.footChoice         = m_footCBox->currentText().toStdString();

    m_walkPathPub.publish(msg);


    // Change ui as needed
    // When manual step generator...
    if(std::strcmp(m_stepGenCBox->currentText().toStdString().c_str(), SG_MANUAL_STR.c_str())==0)
    {
        m_sendPathButton->setText("Queue Step");
    }
    else
    {
        m_sendPathButton->setText("Send Path");
    }

//    // When crawl walk commander
//    if(std::strcmp(m_walkCommanderCBox->currentText().toStdString().c_str(), WC_CRAWL_STR.c_str())==0)
//    {
//        for(int i=0; i<5; i++)
//        {
//            m_footCBox->removeItem(i);
//        }
////        m_footCBox->removeItem(1);
////        m_footCBox->removeItem(2);
//        m_footCBox->addItem(tr(FT_LEFT_STR.c_str()));
//        m_footCBox->addItem(tr(FT_RIGHT_STR.c_str()));
//    }
//    else
//    {
//        for(int i=0; i<5; i++)
//        {
//            m_footCBox->removeItem(i);
//        }
//        m_footCBox->addItem(tr(FT_FORWARD_STR.c_str()));
//        m_footCBox->addItem(tr(FT_BACKWARD_STR.c_str()));
//    }
}

/**
 * Required for rviz plug-in
 */
void WalkPathPlugin::saveToConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config ){}

/**
 * Required for rviz plug-in
 */
void WalkPathPlugin::loadFromConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config ){}

/**
 * Required for rviz plug-in
 */
void WalkPathPlugin::onInitialize(){}


// Tell pluginlib about this class.  Every class which should be
// loadable by pluginlib::ClassLoader must have these two lines
// compiled in its .cpp file, outside of any namespace scope.
#include <pluginlib/class_list_macros.h>
PLUGINLIB_DECLARE_CLASS( re2uta_ocuPlugins, WalkPath, WalkPathPlugin, rviz::Panel )

