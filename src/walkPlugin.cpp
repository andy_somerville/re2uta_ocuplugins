/**
 *
 * walkWidget: this will handle Qt stuff
 *
 * ...
 *
 * @author Isura Ranatunga, University of Texas at Arlington, Copyright (C) 2013.
 * @contact isura.ranatunga@mavs.uta.edu
 * @see https://drc.resquared.com/trac/browser/sandboxes/david_rusbarsky/re2uta_ocuPlugins/src/NNPlugin.cpp
 * @created 03/16/2013
 * @modified 05/21/2013
 *
 */

#include <re2uta_ocuPlugins/walkPlugin.h>

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <std_msgs/Float64.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Empty.h>

walkPlugin::walkPlugin()
{
        // Create the layouts
        QHBoxLayout *a1hbox = new QHBoxLayout();
        QHBoxLayout *a2hbox = new QHBoxLayout();
        QHBoxLayout *a3hbox = new QHBoxLayout();
        QHBoxLayout *a4hbox = new QHBoxLayout();
        QHBoxLayout *a5hbox = new QHBoxLayout();
        QHBoxLayout *a6hbox = new QHBoxLayout();
        QHBoxLayout *a7hbox = new QHBoxLayout();
        QHBoxLayout *a8hbox = new QHBoxLayout();
        QHBoxLayout *a9hbox = new QHBoxLayout();
        QHBoxLayout *a10hbox = new QHBoxLayout();
        QVBoxLayout *vbox1   = new QVBoxLayout(this);
        QVBoxLayout *vbox2   = new QVBoxLayout(this);

        // Create the labels
        m_a1Label = new QLabel("Reset Pose");
        m_a2Label = new QLabel("Reset Floor");
        m_a3Label = new QLabel("Reset Occup");
        m_a4Label = new QLabel("Start Traj");
        m_a5Label = new QLabel("Pause Traj");
        m_a6Label = new QLabel("Stop Traj");
        m_a7Label = new QLabel("KDL");
        m_a8Label = new QLabel("OpenRAVE");
        m_a9Label = new QLabel("AJS Solver");
        m_a10Label = new QLabel("Reset NNs");

        // Create and setup the sping boxes
        m_a1Term = new QPushButton(m_a1Label->text());
        m_a2Term = new QPushButton(m_a2Label->text());
        m_a3Term = new QPushButton(m_a3Label->text());
        m_a4Term = new QPushButton(m_a4Label->text());
        m_a5Term = new QPushButton(m_a5Label->text());
        m_a6Term = new QPushButton(m_a6Label->text());
        m_a7Term = new QRadioButton(m_a7Label->text());
        m_a8Term = new QRadioButton(m_a8Label->text());
        m_a9Term = new QRadioButton(m_a9Label->text());
        m_a10Term = new QPushButton(m_a10Label->text());

        // By default OpenRAVE is used
        m_a7Term->setChecked(true);

//      m_a1Term->setSingleStep(0.01);
//      m_a2Term->setSingleStep(0.01);
//      m_a3Term->setSingleStep(0.01);
//      m_a4Term->setSingleStep(0.01);
//      m_a5Term->setSingleStep(0.01);
//        m_a6Term->setSingleStep(0.01);
        m_a7Term->setStatusTip("Choose KDL, OpenRAVE or AJS as the IK solver");
//        m_a8Term->setSingleStep(0.01);

//        // Add the labels to the layouts
//        a1hbox->addWidget(m_a1Label);
//        a2hbox->addWidget(m_a2Label);
//        a3hbox->addWidget(m_a3Label);
//        a4hbox->addWidget(m_a4Label);
//        a5hbox->addWidget(m_a5Label);
//        a6hbox->addWidget(m_a6Label);
//        a7hbox->addWidget(m_a7Label);
//        a8hbox->addWidget(m_a8Label);

        // Add the spin boxes to the layouts
        a1hbox->addWidget(m_a1Term);
        a2hbox->addWidget(m_a2Term);
        a3hbox->addWidget(m_a3Term);
        a4hbox->addWidget(m_a4Term);
        a5hbox->addWidget(m_a5Term);
        a6hbox->addWidget(m_a6Term);
        a7hbox->addWidget(m_a7Term);
        a8hbox->addWidget(m_a8Term);
        a9hbox->addWidget(m_a9Term);
        a9hbox->addWidget(m_a9Term);
        a10hbox->addWidget(m_a10Term);

        // Add the horizontal layouts to the vertical
        vbox1->addLayout(a1hbox);
        vbox1->addLayout(a2hbox);
        vbox1->addLayout(a3hbox);
        vbox1->addLayout(a4hbox);
        vbox1->addLayout(a5hbox);
        vbox1->addLayout(a6hbox);
        vbox1->addLayout(a7hbox);
        vbox1->addLayout(a8hbox);
        vbox1->addLayout(a9hbox);
        vbox1->addLayout(a10hbox);

        m_timer = m_node.createTimer(ros::Duration( 0.1), &walkPlugin::update, this);

        // Publishers
        traj_pub_ = m_node.advertise< std_msgs::Int32 >( "joint_trajectory_startStop", 1, this);
        pose_pub_ = m_node.advertise< std_msgs::Empty >( "atlas_poseReset", 1, this);
        floo_pub_ = m_node.advertise< std_msgs::Float64 >( "atlas_flooReset", 1, this);
        occu_pub_ = m_node.advertise< std_msgs::Float64 >( "atlas_occuReset", 1, this);
        solv_pub_ = m_node.advertise< std_msgs::Int32 >( "atlas_solvSet", 1, this);
        debug_pub_ = m_node.advertise< std_msgs::Int32 >( "atlas_debugSet", 1, this);
        nn_pub_ = m_node.advertise< std_msgs::Empty >( "atlas_nnReset", 1, this);

        show();
}

void walkPlugin::update( const ros::TimerEvent& e)
{
        std_msgs::Float64 flooReset, occuReset;
        std_msgs::Int32 startStop, solvSet, debugSet;
        std_msgs::Empty poseReset, nnReset;

//        msg.data.push_back(m_a1Term->isDown()));
//        msg.data.push_back(m_a2Term->value()));
//        msg.data.push_back(((double)m_a3Term->value()));
//        msg.data.push_back(         m_a4Term->value());
//        msg.data.push_back(         m_a5Term->value());
//        msg.data.push_back(         m_a6Term->value());
//        msg.data.push_back(         m_a7Term->value());
//        msg.data.push_back(         m_a8Term->value());

        // Publish the robot trajectory preempt messages
        if(m_a4Term->isDown())
        {
          startStop.data = 2;
          traj_pub_.publish(startStop);
        }else if(m_a5Term->isDown())
        {
          startStop.data = 1;
          traj_pub_.publish(startStop);
        }else if(m_a6Term->isDown())
        {
          startStop.data = 3;
          traj_pub_.publish(startStop);
        }else{
          startStop.data = 0;
          traj_pub_.publish(startStop);
        }

        // Publish the pose reset message
        if(m_a1Term->isDown())
        {
          pose_pub_.publish(poseReset);
        }

        // Publish the floor plane reset message
        if(m_a2Term->isDown())
        {
          flooReset.data = 1;
          floo_pub_.publish(flooReset);
        }

        // Publish the occupancy grid reset message
        if(m_a3Term->isDown())
        {
          occuReset.data = 1;
          occu_pub_.publish(occuReset);
        }

        // Publish the solver set message
        if(m_a7Term->isChecked())
        {
          solvSet.data = 1; // 1 - for KDL
                            // 0 - for OpenRAVE (default)
          solv_pub_.publish(solvSet);
        }else if(m_a8Term->isChecked()){
          solvSet.data = 0; // 1 - for KDL
                            // 0 - for OpenRAVE (default)
          solv_pub_.publish(solvSet);
        }else if(m_a9Term->isChecked()){
          solvSet.data = 2; // 1 - for KDL
                            // 0 - for OpenRAVE (default)
          solv_pub_.publish(solvSet);
        }

        // Publish the nn reset message
        if(m_a10Term->isDown())
        {
          nn_pub_.publish(nnReset);
        }


//        // Publish the debug set message
//        if(m_a9Term->isChecked())
//        {
//          debugSet.data = 1; // 1 - for KDL
//                            // 0 - for OpenRAVE (default)
//          debug_pub_.publish(debugSet);
//        }else{
//          solvSet.data = 0; // 1 - for KDL
//                            // 0 - for OpenRAVE (default)
//          debug_pub_.publish(debugSet);
//        }

}

/**
 * Required for rviz plug-in
 */
void walkPlugin::saveToConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config )
{
        config->set("nn_a1",                   m_a1Term->isDown());
        config->set("nn_a2",                   m_a2Term->isDown());
        config->set("nn_a3",                   m_a3Term->isDown());
        config->set("nn_a4",                   m_a4Term->isDown());
        config->set("nn_a5",                   m_a5Term->isDown());
        config->set("nn_a6",                   m_a6Term->isDown());
        config->set("nn_a7",static_cast<int>(m_a7Term->isChecked()));
        config->set("nn_a8",static_cast<int>(m_a8Term->isChecked()));
        config->set("nn_a9",static_cast<int>(m_a9Term->isChecked()));
}

/**
 * Required for rviz plug-in
 */
void walkPlugin::loadFromConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config )
{
        int   iVal;
        float fVal;
        int checked;

//        config->get("nn_a1", &iVal, 0);
//        m_a1Term->setValue(iVal);

//        config->get("nn_a2", &iVal, 0);
//        m_a2Term->setValue(iVal);

//        config->get("nn_a3", &iVal, 0);
//        m_a3Term->setValue(iVal);

//        config->get("nn_a4", &fVal, 0.0);
//        m_a4Term->setValue(static_cast<double>(fVal));

//        config->get("nn_a5", &fVal, 0.0);
//        m_a5Term->setValue(static_cast<double>(fVal));

//        config->get("nn_a6", &fVal, 0.0);
//        m_a6Term->setValue(static_cast<double>(fVal));

        config->get("nn_a7", &checked, 0);
        m_a7Term->setChecked(static_cast<bool>(checked));

        config->get("nn_a8", &checked, 0);
        m_a8Term->setChecked(static_cast<bool>(checked));

        config->get("nn_a9", &checked, 0);
        m_a9Term->setChecked(static_cast<bool>(checked));
}

/**
 * Required for rviz plug-in
 */
void walkPlugin::onInitialize()
{
}

// Tell pluginlib about this class.  Every class which should be
// loadable by pluginlib::ClassLoader must have these two lines
// compiled in its .cpp file, outside of any namespace scope.
#include <pluginlib/class_list_macros.h>
PLUGINLIB_DECLARE_CLASS( re2uta_ocuPlugins, Walk, walkPlugin, rviz::Panel )
