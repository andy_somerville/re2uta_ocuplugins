/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <QHBoxLayout>
#include <re2uta_ocuPlugins/LaserRotPlugin.h>
#include <std_msgs/Float64.h>

LaserRotPlugin::LaserRotPlugin()
{
    // Create the layouts
    QHBoxLayout *a1hbox = new QHBoxLayout(this);
    m_rateBox = new QDoubleSpinBox();
    m_setRateButton = new QPushButton("Set Rate", this);

    m_rateBox->setMinimum(0.0);
    m_rateBox->setMaximum(5.0);
    m_rateBox->setSingleStep(0.1);

    double tempRateSpeed;
    m_node.param<double>( "LaserRotHandler_defaultRotSpeed", tempRateSpeed, 1.2 );
    m_rateBox->setValue(tempRateSpeed);

    a1hbox->addWidget(m_rateBox);
    a1hbox->addWidget(m_setRateButton);

    // Connect button signal to appropriate slot
    connect(m_setRateButton, SIGNAL(released()), this, SLOT(handleButton()));

    // Publishers
    m_setRatePub = m_node.advertise< std_msgs::Float64 >( "/ui/laserRot", 1, this);

    show();
}

void LaserRotPlugin::handleButton()
{
    std_msgs::Float64 msg;
    msg.data = m_rateBox->value();
    m_setRatePub.publish(msg);
}

void LaserRotPlugin::saveToConfig(   const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config ){}
void LaserRotPlugin::loadFromConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config ){}
void LaserRotPlugin::onInitialize(){}

#include <pluginlib/class_list_macros.h>
PLUGINLIB_DECLARE_CLASS( re2uta_ocuPlugins, LaserRot, LaserRotPlugin, rviz::Panel )
