/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <re2uta_ocuPlugins/FrameCmdPlugin.h>
#include <QHBoxLayout>
#include <QVBoxLayout>

static const std::string HEAD_STR("head");
static const std::string L_CLAV_STR("l_clav");
static const std::string L_FARM_STR("l_farm");
static const std::string L_FOOT_STR("l_foot");
static const std::string L_HAND_STR("l_hand");
static const std::string L_LARM_STR("l_larm");
static const std::string L_LGLUT_STR("l_lglut");
static const std::string L_LLEG_STR("l_lleg");
static const std::string L_SCAP_STR("l_scap");
static const std::string L_TALUS_STR("l_talus");
static const std::string L_UARM_STR("l_uarm");
static const std::string L_UGLUT_STR("l_uglut");
static const std::string L_ULEG_STR("l_uleg");
static const std::string LTORSO_STR("ltorso");
static const std::string MTORSO_STR("mtorso");
static const std::string PELVIS_STR("pelvis");
static const std::string R_CLAV_STR("r_clav");
static const std::string R_FARM_STR("r_farm");
static const std::string R_FOOT_STR("r_foot");
static const std::string R_HAND_STR("r_hand");
static const std::string R_LARM_STR("r_larm");
static const std::string R_LGLUT_STR("r_lglut");
static const std::string R_LLEG_STR("r_lleg");
static const std::string R_SCAP_STR("r_scap");
static const std::string R_TALUS_STR("r_talus");
static const std::string R_UARM_STR("r_uarm");
static const std::string R_UGLUT_STR("r_uglut");
static const std::string R_ULEG_STR("r_uleg");
static const std::string UTORSO_STR("utorso");
static const std::string HOSE_STR("hose");
static const std::string PIPE_STR("pipe");
static const std::string VALVE_STR("value");

FrameCmdPlugin::FrameCmdPlugin()
{
    // Create the labels
    m_label[0] = new QLabel("Base");
    m_label[1] = new QLabel("Tip");
    m_label[2] = new QLabel("Goal");
    m_label[3] = new QLabel("X");
    m_label[4] = new QLabel("Y");
    m_label[5] = new QLabel("Z");
    m_label[6] = new QLabel("Rot X");
    m_label[7] = new QLabel("Rot Y");
    m_label[8] = new QLabel("Rot Z");

    // Create the spin boxes
    for (int i=0; i<3; i++) {
        m_term[i] = new QDoubleSpinBox();
        (m_term[i])->setSingleStep(0.1);
        (m_term[i])->setDecimals(3);
        (m_term[i])->setRange(-16.0, 16.0);
    }

    for (int i=3; i<6; i++) {
        m_term[i] = new QDoubleSpinBox();
        (m_term[i])->setSingleStep(1);
        (m_term[i])->setDecimals(0);
        (m_term[i])->setRange(-180.0, 180.0);
    }

    // Create and setup the combo boxes
    for (int i=0; i<3; i++) {
        m_combo[i] = new QComboBox();
        (m_combo[i])->addItem(tr(HEAD_STR.c_str()));
        (m_combo[i])->addItem(tr(L_CLAV_STR.c_str()));
        (m_combo[i])->addItem(tr(L_FARM_STR.c_str()));
        (m_combo[i])->addItem(tr(L_FOOT_STR.c_str()));
        (m_combo[i])->addItem(tr(L_HAND_STR.c_str()));
        (m_combo[i])->addItem(tr(L_LARM_STR.c_str()));
        (m_combo[i])->addItem(tr(L_LGLUT_STR.c_str()));
        (m_combo[i])->addItem(tr(L_LLEG_STR.c_str()));
        (m_combo[i])->addItem(tr(L_SCAP_STR.c_str()));
        (m_combo[i])->addItem(tr(L_TALUS_STR.c_str()));
        (m_combo[i])->addItem(tr(L_UARM_STR.c_str()));
        (m_combo[i])->addItem(tr(L_UGLUT_STR.c_str()));
        (m_combo[i])->addItem(tr(L_ULEG_STR.c_str()));
        (m_combo[i])->addItem(tr(LTORSO_STR.c_str()));
        (m_combo[i])->addItem(tr(MTORSO_STR.c_str()));
        (m_combo[i])->addItem(tr(PELVIS_STR.c_str()));
        (m_combo[i])->addItem(tr(R_CLAV_STR.c_str()));
        (m_combo[i])->addItem(tr(R_FARM_STR.c_str()));
        (m_combo[i])->addItem(tr(R_FOOT_STR.c_str()));
        (m_combo[i])->addItem(tr(R_HAND_STR.c_str()));
        (m_combo[i])->addItem(tr(R_LARM_STR.c_str()));
        (m_combo[i])->addItem(tr(R_LGLUT_STR.c_str()));
        (m_combo[i])->addItem(tr(R_LLEG_STR.c_str()));
        (m_combo[i])->addItem(tr(R_SCAP_STR.c_str()));
        (m_combo[i])->addItem(tr(R_TALUS_STR.c_str()));
        (m_combo[i])->addItem(tr(R_UARM_STR.c_str()));
        (m_combo[i])->addItem(tr(R_UGLUT_STR.c_str()));
        (m_combo[i])->addItem(tr(R_ULEG_STR.c_str()));
        (m_combo[i])->addItem(tr(UTORSO_STR.c_str()));
        (m_combo[i])->addItem(tr(HOSE_STR.c_str()));
        (m_combo[i])->addItem(tr(PIPE_STR.c_str()));
        (m_combo[i])->addItem(tr(VALVE_STR.c_str()));
    }

    // slider/spin for weights
    for (int i=0; i<6; i++) {
        m_weightSlider[i] = new SliderAndSpinBox();
        (m_weightSlider[i])->setRange(0, 50);
    }

    // and the button
    m_sendButton = new QPushButton("SEND CMD", this);
    m_resetButton = new QPushButton("RESET", this);

    // Connect button signals to appropriate slot
    connect(m_sendButton, SIGNAL(released()), this, SLOT(sendButton()));
    connect(m_resetButton, SIGNAL(released()), this, SLOT(resetButton()));

    int numHBox = 4;
    QHBoxLayout *hbox[numHBox];
    QVBoxLayout *vbox = new QVBoxLayout(this);

    for (int i=0; i<numHBox; i++) {
        hbox[i] = new QHBoxLayout();
    }

    (hbox[0])->addWidget(m_label[0]);
    (hbox[0])->addWidget(m_combo[0]);
    (hbox[0])->addWidget(m_label[1]);
    (hbox[0])->addWidget(m_combo[1]);
    (hbox[0])->addWidget(m_label[2]);
    (hbox[0])->addWidget(m_combo[2]);
    (hbox[0])->addWidget(m_resetButton);
    (hbox[0])->addWidget(m_sendButton);

    (hbox[1])->addWidget(m_label[3]);
    (hbox[1])->addWidget(m_term[0]);
    (hbox[1])->addWidget(m_weightSlider[0]);
    (hbox[1])->addWidget(m_label[6]);
    (hbox[1])->addWidget(m_term[3]);
    (hbox[1])->addWidget(m_weightSlider[3]);

    (hbox[2])->addWidget(m_label[4]);
    (hbox[2])->addWidget(m_term[1]);
    (hbox[2])->addWidget(m_weightSlider[1]);
    (hbox[2])->addWidget(m_label[7]);
    (hbox[2])->addWidget(m_term[4]);
    (hbox[2])->addWidget(m_weightSlider[4]);

    (hbox[3])->addWidget(m_label[5]);
    (hbox[3])->addWidget(m_term[2]);
    (hbox[3])->addWidget(m_weightSlider[2]);
    (hbox[3])->addWidget(m_label[8]);
    (hbox[3])->addWidget(m_term[5]);
    (hbox[3])->addWidget(m_weightSlider[5]);

    for (int i=0; i<numHBox; i++) {
        vbox->addLayout(hbox[i]);
    }

    show();

    m_cmdPub = m_node.advertise< re2uta_atlasCommander::FrameCommandStamped >( "/ui/frame_cmd", 1, this);

    m_msg.goalPose.pose.position.x = 0;
    m_msg.goalPose.pose.position.y = 0;
    m_msg.goalPose.pose.position.z = 0;
    m_msg.goalPose.pose.orientation.x = 0;
    m_msg.goalPose.pose.orientation.y = 0;
    m_msg.goalPose.pose.orientation.z = 0;

    m_msg.weights.linear.x = 0;
    m_msg.weights.linear.y = 0;
    m_msg.weights.linear.z = 0;
    m_msg.weights.angular.x = 0;
    m_msg.weights.angular.y = 0;
    m_msg.weights.angular.z = 0;
}


void FrameCmdPlugin::sendButton()
{
    // frames
    m_msg.header.frame_id = (m_combo[0])->currentText().toStdString();
    m_msg.target_frame_id = (m_combo[1])->currentText().toStdString();
    m_msg.goalPose.header.frame_id = (m_combo[2])->currentText().toStdString();

    // weights
    m_msg.weights.linear.x = (m_weightSlider[0])->getSpinBoxVal() * 0.01;
    m_msg.weights.linear.y = (m_weightSlider[1])->getSpinBoxVal() * 0.01;
    m_msg.weights.linear.z = (m_weightSlider[2])->getSpinBoxVal() * 0.01;
    m_msg.weights.angular.x = (m_weightSlider[3])->getSpinBoxVal() * 0.01;
    m_msg.weights.angular.y = (m_weightSlider[4])->getSpinBoxVal() * 0.01;
    m_msg.weights.angular.z = (m_weightSlider[5])->getSpinBoxVal() * 0.01;

    // pose
    m_msg.goalPose.pose.position.x = (m_term[0])->value();
    m_msg.goalPose.pose.position.y = (m_term[1])->value();
    m_msg.goalPose.pose.position.z = (m_term[2])->value();

#if 0
    // CONVERT x,y,z ROTATION TO QUATERNION!
    // first convert to a rotation matrix
    double r[9];
    double cx, cy, cz, sx, sy, sz;
    cx = cos(rotx);
    sx = sin(rotx);
    cy = cos(roty);
    sy = sin(roty);
    cz = cos(rotz);
    sz = sin(rotz);

    r[0] = cz*cy;
    r[1] = cz*sy*sx - sz*cx;
    r[2] = cz*sy*cx + sz*sx;

    r[3] = sz*cy;
    r[4] = sz*sy*sx + cz*cx;
    r[5] = sz*sy*cx - cz*sx;

    r[6] = -sy;
    r[7] = cy*sx;
    r[8] = cy*cx;

    double w;

    w = 0.5 * sqrt(1 + r[0] + r[4] + r[8]);

    m_frameCmdToPublish.goalPose.pose.orientation.x = (r[7]-r[5])/w;
    m_frameCmdToPublish.goalPose.pose.orientation.y = (r[2]-r[6])/w;
    m_frameCmdToPublish.goalPose.pose.orientation.z = (r[3]-r[1])/w;
    m_frameCmdToPublish.goalPose.pose.orientation.w = w;
#endif

    m_msg.goalPose.pose.orientation.x = (m_term[3])->value()*M_PI/180;
    m_msg.goalPose.pose.orientation.y = (m_term[4])->value()*M_PI/180;
    m_msg.goalPose.pose.orientation.z = (m_term[5])->value()*M_PI/180;

    ROS_INFO("SENDING MSG");
    ROS_INFO("BASE FRAME: %s", m_msg.header.frame_id.c_str());
    ROS_INFO("TIP FRAME: %s", m_msg.target_frame_id.c_str());
    ROS_INFO("GOAL FRAME: %s", m_msg.goalPose.header.frame_id.c_str());
    ROS_INFO("WEIGHTS: %g %g %g %g %g %g",
             m_msg.weights.linear.x,
             m_msg.weights.linear.y,
             m_msg.weights.linear.z,
             m_msg.weights.angular.x,
             m_msg.weights.angular.y,
             m_msg.weights.angular.z);
    ROS_INFO("POSE: %g %g %g %g %g %g",
             m_msg.goalPose.pose.position.x,
             m_msg.goalPose.pose.position.y,
             m_msg.goalPose.pose.position.z,
             m_msg.goalPose.pose.orientation.x,
             m_msg.goalPose.pose.orientation.y,
             m_msg.goalPose.pose.orientation.z);

    m_cmdPub.publish(m_msg);
}

void FrameCmdPlugin::resetButton()
{
    m_msg.goalPose.pose.position.x = 0.0;
    m_msg.goalPose.pose.position.y = 0.0;
    m_msg.goalPose.pose.position.z = 0.0;
    m_msg.goalPose.pose.orientation.x = 0.0;
    m_msg.goalPose.pose.orientation.y = 0.0;
    m_msg.goalPose.pose.orientation.z = 0.0;

    (m_term[0])->setValue(m_msg.goalPose.pose.position.x);
    (m_term[1])->setValue(m_msg.goalPose.pose.position.y);
    (m_term[2])->setValue(m_msg.goalPose.pose.position.z);
    (m_term[3])->setValue(m_msg.goalPose.pose.orientation.x*180/M_PI);
    (m_term[4])->setValue(m_msg.goalPose.pose.orientation.y*180/M_PI);
    (m_term[5])->setValue(m_msg.goalPose.pose.orientation.z*180/M_PI);
}

/**
 * Required for rviz plug-in
 */
void FrameCmdPlugin::saveToConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config )
{
    config->set( "FrameCmdHandler_baseFrame", (m_combo[0])->currentText().toStdString() );
    config->set( "FrameCmdHandler_tipFrame", (m_combo[1])->currentText().toStdString() );
    config->set( "FrameCmdHandler_goalFrame", (m_combo[2])->currentText().toStdString() );
}

/**
 * Required for rviz plug-in
 */
void FrameCmdPlugin::loadFromConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config )
{
#if 1
    std::string baseFrame;
    std::string tipFrame;
    std::string goalFrame;
    int index;

    config->get( "FrameCmdHandler_baseFrame", &baseFrame, "DEFAULT VALUE" );
    config->get( "FrameCmdHandler_tipFrame", &tipFrame, "DEFAULT VALUE" );
    config->get( "FrameCmdHandler_goalFrame", &goalFrame, "DEFAULT VALUE" );

    index = (m_combo[0])->findText(baseFrame.c_str());
    if ( index != -1 ) { // -1 for not found
        (m_combo[0])->setCurrentIndex(index);
    }

    index = (m_combo[1])->findText(tipFrame.c_str());
    if ( index != -1 ) { // -1 for not found
        (m_combo[1])->setCurrentIndex(index);
    }

    index = (m_combo[2])->findText(goalFrame.c_str());
    if ( index != -1 ) { // -1 for not found
        (m_combo[2])->setCurrentIndex(index);
    }
#endif
}

/**
 * Required for rviz plug-in
 */
void FrameCmdPlugin::onInitialize()
{
}

// Tell pluginlib about this class.  Every class which should be
// loadable by pluginlib::ClassLoader must have these two lines
// compiled in its .cpp file, outside of any namespace scope.
#include <pluginlib/class_list_macros.h>
PLUGINLIB_DECLARE_CLASS( re2uta_ocuPlugins, FrameCmd, FrameCmdPlugin, rviz::Panel )

