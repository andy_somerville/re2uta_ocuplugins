/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <re2uta_ocuPlugins/OdomPlugin.h>

#include <QHBoxLayout>

#include <std_msgs/Int8.h>

static const uint8_t RE2UTA_REQ_ODOM     = 1;
static const uint8_t RE2UTA_REQ_VIZ_ODOM = 2;
static const uint8_t RE2UTA_REQ_KIN_ODOM = 3;

OdomPlugin::OdomPlugin()
{
    QHBoxLayout *hbox = new QHBoxLayout(this);

//    m_reqOdomButton    = new QPushButton("Req Odom", this);
    m_reqVizOdomButton = new QPushButton("Req Viz Odom",  this);
    m_reqKinOdomButton = new QPushButton("Req Kin Odom",  this);

//    hbox->addWidget(m_reqOdomButton);
    hbox->addWidget(m_reqVizOdomButton);
    hbox->addWidget(m_reqKinOdomButton);

    setLayout(hbox);

//    connect(m_reqOdomButton,    SIGNAL(released()), this, SLOT(requestOdom()   ) );
    connect(m_reqVizOdomButton, SIGNAL(released()), this, SLOT(requestVizOdom()) );
    connect(m_reqKinOdomButton, SIGNAL(released()), this, SLOT(requestKinOdom()) );

    m_reqPub = m_node.advertise< std_msgs::Int8 >( "/ui/reqOdom", 1, this);

    m_reqVizOdomButton->setEnabled(false);
    show();
    requestKinOdom();
}

//void OdomPlugin::requestOdom()
//{
//    std_msgs::Int8 msg;
//    msg.data = RE2UTA_REQ_ODOM;
//
//    m_reqPub.publish(msg);
//}

void OdomPlugin::requestVizOdom()
{
    std_msgs::Int8 msg;
    msg.data = RE2UTA_REQ_VIZ_ODOM;

    m_reqPub.publish(msg);
}

void OdomPlugin::requestKinOdom()
{
    std_msgs::Int8 msg;
    msg.data = RE2UTA_REQ_KIN_ODOM;

    m_reqPub.publish(msg);
}

/**
 * Required for rviz plug-in
 */
void OdomPlugin::saveToConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config ){}

/**
 * Required for rviz plug-in
 */
void OdomPlugin::loadFromConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config ){}

/**
 * Required for rviz plug-in
 */
void OdomPlugin::onInitialize(){}


// Tell pluginlib about this class.  Every class which should be
// loadable by pluginlib::ClassLoader must have these two lines
// compiled in its .cpp file, outside of any namespace scope.
#include <pluginlib/class_list_macros.h>
PLUGINLIB_DECLARE_CLASS( re2uta_ocuPlugins, Odom, OdomPlugin, rviz::Panel )

