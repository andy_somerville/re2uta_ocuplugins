/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <re2uta_ocuPlugins/DataPlugin.h>
#include <QHBoxLayout>
#include <QVBoxLayout>

static const std::string DATA_SCENARIO_1_STR("1 (115,200 / 58,982,400)");
static const std::string DATA_SCENARIO_2_STR("2 (460,800 / 117,964,800)");
static const std::string DATA_SCENARIO_3_STR("3 (1,843,200 / 235,929,600)");
static const std::string DATA_SCENARIO_4_STR("4 (7,373,800 / 471,859,200)");
static const std::string DATA_SCENARIO_5_STR("5 (29,491,200 / 943,718,400)");

static const int DATA_SCENARIO_1_MAX_UP = 115200;
static const int DATA_SCENARIO_2_MAX_UP = 460800;
static const int DATA_SCENARIO_3_MAX_UP = 1843200;
static const int DATA_SCENARIO_4_MAX_UP = 7373800;
static const int DATA_SCENARIO_5_MAX_UP = 29491200;

static const int DATA_SCENARIO_1_MAX_DOWN = 58982400;
static const int DATA_SCENARIO_2_MAX_DOWN = 117964800;
static const int DATA_SCENARIO_3_MAX_DOWN = 235929600;
static const int DATA_SCENARIO_4_MAX_DOWN = 471859200;
static const int DATA_SCENARIO_5_MAX_DOWN = 943718400;

DataPlugin::DataPlugin()
{
    // Create the layouts
    QHBoxLayout *hBoxLayout = new QHBoxLayout(this);
    QVBoxLayout *vbox       = new QVBoxLayout();
    QVBoxLayout *vbox2      = new QVBoxLayout();

    // Create and setup the combo boxes
    m_scenario = new QComboBox();
    m_scenario->addItem(tr(DATA_SCENARIO_1_STR.c_str()));
    m_scenario->addItem(tr(DATA_SCENARIO_2_STR.c_str()));
    m_scenario->addItem(tr(DATA_SCENARIO_3_STR.c_str()));
    m_scenario->addItem(tr(DATA_SCENARIO_4_STR.c_str()));
    m_scenario->addItem(tr(DATA_SCENARIO_5_STR.c_str()));

    m_update = new QPushButton("Update", this);

    m_dataProgressUp   = new QProgressBar();
    m_dataProgressDown = new QProgressBar();
    m_timeProgress     = new QProgressBar();

    m_timeProgress->setMaximum(30);
    m_timeProgress->setValue(0);

    // show text "currentValue/totalValue (percent)" on progress bar
    m_dataProgressUp->setFormat(   "Data Up Used: %v/%m (%p%)" );
    m_dataProgressDown->setFormat( "Data Down Used: %v/%m (%p%)" );
    m_timeProgress->setFormat(     "Minutes Used: %v/%m (%p%)" );

    // Add the widgets to the layout
    vbox->addWidget(m_timeProgress);
    vbox->addWidget(m_dataProgressUp);
    vbox->addWidget(m_dataProgressDown);
    vbox2->addWidget(m_scenario);
    vbox2->addWidget(m_update);
    hBoxLayout->addLayout(vbox2);
    hBoxLayout->addLayout(vbox);

    // TODO: Have RMC publish their data usage. This class subscribes to it.

    // Connect button signal to appropriate slot
    connect(m_scenario, SIGNAL(activated(int)), this, SLOT(setDataScenario()));
    connect(m_update, SIGNAL(released()), this, SLOT(updateButton()));

    std::string rosTopicSubDataVal;
    m_node.param<std::string>("/re2uta/ScoreHandler_rosTopicUIData", rosTopicSubDataVal,
                              "/ui/ScoreHandlerData");

    // Create a timer for publishing the most recent img data
    m_timer = m_node.createTimer(ros::Duration( 0.05), &DataPlugin::update, this);

    m_dataUpSub = m_node.subscribe("/ui/dataUp", 1, &DataPlugin::dataUpCB, this );
    m_dataDownSub = m_node.subscribe("/ui/dataDown", 1, &DataPlugin::dataDownCB, this );
    m_scoreSub = m_node.subscribe( rosTopicSubDataVal, 1, &DataPlugin::timeCB, this );

    m_requestPub = m_node.advertise<std_msgs::Empty>("/ui/DataUsageHandlerReq", 1, this);

    // Grab the handler id for ImgHandler
//    int tempHandlerId;
//    m_node.param<int>("/re2uta/ImgHandler_id", tempHandlerId, 0);
//    m_handlerId = tempHandlerId;

    // Publishers
//    m_node.param<std::string>("/re2uta/ImgHandler_rosTopicReqVal", m_rosTopic, "/ui/imgHandlerReq");
//    m_requestPub = m_node.advertise< re2uta_inetComms::ReqImgData >( m_rosTopic, 1, this);
    setDataScenario();

    show();
}

void DataPlugin::dataUpCB( const std_msgs::Int64ConstPtr& msg )
{
    m_dataProgressUp->setValue(msg->data);
}

void DataPlugin::dataDownCB( const std_msgs::Int64ConstPtr& msg )
{
    m_dataProgressDown->setValue(msg->data);
}

void DataPlugin::timeCB( const atlas_msgs::VRCScore& msg )
{
    int time = (int) (msg.sim_time_elapsed.sec / 60.0);

    ROS_INFO("[DataPlugin] Received new time, updating ui: %d", time);

    if(time > 30)
    {
        time = 30;
    }

    m_timeProgress->setValue(time);
}

void DataPlugin::updateButton()
{
  std_msgs::Empty msg;

  m_requestPub.publish(msg);
  
}

void DataPlugin::update( const ros::TimerEvent& e)
{
//    // TODO: Remove (this was for testing only)
//    m_dataProgressUp->setValue(m_dataProgressUp->value()+100);
//    m_dataProgressDown->setValue(m_dataProgressDown->value()+1000);
}

void DataPlugin::setDataScenario()
{
    std::string currentScenario = m_scenario->currentText().toStdString();
    if(currentScenario == DATA_SCENARIO_1_STR)
    {
        if(m_dataProgressUp->value() > DATA_SCENARIO_1_MAX_UP)
        {
            m_dataProgressUp->setValue(DATA_SCENARIO_1_MAX_UP);
        }
        m_dataProgressUp->setMaximum(DATA_SCENARIO_1_MAX_UP);

        if(m_dataProgressDown->value() > DATA_SCENARIO_1_MAX_DOWN)
        {
            m_dataProgressDown->setValue(DATA_SCENARIO_1_MAX_DOWN);
        }
        m_dataProgressDown->setMaximum(DATA_SCENARIO_1_MAX_DOWN);

        ROS_INFO("Setting Data Scenario: %s",DATA_SCENARIO_1_STR.c_str());
    }
    else if(currentScenario == DATA_SCENARIO_2_STR)
    {
        if(m_dataProgressUp->value() > DATA_SCENARIO_2_MAX_UP)
        {
            m_dataProgressUp->setValue(DATA_SCENARIO_2_MAX_UP);
        }
        m_dataProgressUp->setMaximum(DATA_SCENARIO_2_MAX_UP);

        if(m_dataProgressDown->value() > DATA_SCENARIO_2_MAX_DOWN)
        {
            m_dataProgressDown->setValue(DATA_SCENARIO_2_MAX_DOWN);
        }
        m_dataProgressDown->setMaximum(DATA_SCENARIO_2_MAX_DOWN);

        ROS_INFO("Setting Data Scenario: %s",DATA_SCENARIO_2_STR.c_str());
    }
    else if(currentScenario == DATA_SCENARIO_3_STR)
    {
        if(m_dataProgressUp->value() > DATA_SCENARIO_3_MAX_UP)
        {
            m_dataProgressUp->setValue(DATA_SCENARIO_3_MAX_UP);
        }
        m_dataProgressUp->setMaximum(DATA_SCENARIO_3_MAX_UP);

        if(m_dataProgressDown->value() > DATA_SCENARIO_3_MAX_DOWN)
        {
            m_dataProgressDown->setValue(DATA_SCENARIO_3_MAX_DOWN);
        }
        m_dataProgressDown->setMaximum(DATA_SCENARIO_3_MAX_DOWN);

        ROS_INFO("Setting Data Scenario: %s",DATA_SCENARIO_3_STR.c_str());
    }
    else if(currentScenario == DATA_SCENARIO_4_STR)
    {
        if(m_dataProgressUp->value() > DATA_SCENARIO_4_MAX_UP)
        {
            m_dataProgressUp->setValue(DATA_SCENARIO_4_MAX_UP);
        }
        m_dataProgressUp->setMaximum(DATA_SCENARIO_4_MAX_UP);

        if(m_dataProgressDown->value() > DATA_SCENARIO_4_MAX_DOWN)
        {
            m_dataProgressDown->setValue(DATA_SCENARIO_4_MAX_DOWN);
        }
        m_dataProgressDown->setMaximum(DATA_SCENARIO_4_MAX_DOWN);

        ROS_INFO("Setting Data Scenario: %s",DATA_SCENARIO_4_STR.c_str());
    }
    else if(currentScenario == DATA_SCENARIO_5_STR)
    {
        if(m_dataProgressUp->value() > DATA_SCENARIO_5_MAX_UP)
        {
            m_dataProgressUp->setValue(DATA_SCENARIO_5_MAX_UP);
        }
        m_dataProgressUp->setMaximum(DATA_SCENARIO_5_MAX_UP);

        if(m_dataProgressDown->value() > DATA_SCENARIO_5_MAX_DOWN)
        {
            m_dataProgressDown->setValue(DATA_SCENARIO_5_MAX_DOWN);
        }
        m_dataProgressDown->setMaximum(DATA_SCENARIO_5_MAX_DOWN);

        ROS_INFO("Setting Data Scenario: %s",DATA_SCENARIO_5_STR.c_str());
    }
    else
    {
        ROS_WARN("[DataPlugin] Unknown scenario: %s",currentScenario.c_str());

        if(m_dataProgressUp->value() > DATA_SCENARIO_1_MAX_UP)
        {
            m_dataProgressUp->setValue(DATA_SCENARIO_1_MAX_UP);
        }
        m_dataProgressUp->setMaximum(DATA_SCENARIO_1_MAX_UP);

        if(m_dataProgressDown->value() > DATA_SCENARIO_1_MAX_DOWN)
        {
            m_dataProgressDown->setValue(DATA_SCENARIO_1_MAX_DOWN);
        }
        m_dataProgressDown->setMaximum(DATA_SCENARIO_1_MAX_DOWN);
    }

}

/**
 * Required for rviz plug-in
 */
void DataPlugin::saveToConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config )
{
    // TODO: Save choice for commLvl
}

/**
 * Required for rviz plug-in
 */
void DataPlugin::loadFromConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config )
{
    // TODO: Save choice for commLvl
}

/**
 * Required for rviz plug-in
 */
void DataPlugin::onInitialize()
{
}

// Tell pluginlib about this class.  Every class which should be
// loadable by pluginlib::ClassLoader must have these two lines
// compiled in its .cpp file, outside of any namespace scope.
#include <pluginlib/class_list_macros.h>
PLUGINLIB_DECLARE_CLASS( re2uta_ocuPlugins, DataProgress, DataPlugin, rviz::Panel )
