/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <re2uta_ocuPlugins/StatsPlugin.h>

#include <QHBoxLayout>
#include <QVBoxLayout>

#include <std_msgs/Bool.h>

StatsPlugin::StatsPlugin()
{
    QVBoxLayout *vbox = new QVBoxLayout(this);
    QHBoxLayout *hbox = new QHBoxLayout();

    m_reqButton       = new QPushButton("Req Stats", this);
    m_autoReqCheckBox = new QCheckBox("Auto Update", this);
    m_scoreLabel      = new QLabel("Loading...");
    m_fallsLabel      = new QLabel("Loading...");

    setScoreLabel(0);
    setFallsLabel(0);

    hbox->addWidget(m_reqButton);
    hbox->addWidget(m_autoReqCheckBox);
    vbox->addLayout(hbox);
    vbox->addWidget(m_scoreLabel);
    vbox->addWidget(m_fallsLabel);

    setLayout(vbox);

    connect(m_reqButton,        SIGNAL(released()), this, SLOT(reqStats())     );
    connect(m_autoReqCheckBox,  SIGNAL(released()), this, SLOT(reqStats()) );

    m_reqPub = m_node.advertise< std_msgs::Bool >( "/ui/reqStats", 1, this);
    m_statsSub  = m_node.subscribe("/ui/stats" , 1, &StatsPlugin::updateStats, this);

    show();
}

void StatsPlugin::reqStats()
{
    std_msgs::Bool msg;
    msg.data = m_autoReqCheckBox->isChecked();
    m_reqPub.publish(msg);
}

//void StatsPlugin::autoReqStats()
//{
//    std_msgs::Bool msg;
//    msg.data = m_autoReqCheckBox->isChecked();
//    m_reqPub.publish(msg);
//}

void StatsPlugin::updateStats(const atlas_msgs::VRCScore &msg)
{
    int falls = msg.falls;
    int score = msg.completion_score;

    // TODO: Set labels
    setFallsLabel(falls);
    setScoreLabel(score);
}

void StatsPlugin::setFallsLabel(int falls)
{
    switch(falls)
    {
    case 0:
        m_fallsLabel->setStyleSheet("QLabel { background-color : green; color : black; }");
        m_fallsLabel->setText("Falls: _ _ _");
        break;
    case 1:
        m_fallsLabel->setStyleSheet("QLabel { background-color : yellow; color : black; }");
        m_fallsLabel->setText("Falls: X _ _");
        break;
    case 2:
        m_fallsLabel->setStyleSheet("QLabel { background-color : red; color : black; }");
        m_fallsLabel->setText("Falls: X X _");
        break;
    case 3:
        m_fallsLabel->setStyleSheet("QLabel { background-color : black; color : red; }");
        m_fallsLabel->setText("Falls: X X X");
        break;
    default:
        ROS_WARN("[StatsPlugin] Received a fall count of %d",falls);
        m_fallsLabel->setStyleSheet("QLabel { background-color : red; color : blue; }");
        m_fallsLabel->setText("Falls: ? ? ?");
        break;
    }
}

void StatsPlugin::setScoreLabel(int score)
{
    std::stringstream ss;
//    QString scoreQstring;
    ss << "Score: " << score;

    m_scoreLabel->setText(ss.str().c_str());
}

/**
 * Required for rviz plug-in
 */
void StatsPlugin::saveToConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config ){}

/**
 * Required for rviz plug-in
 */
void StatsPlugin::loadFromConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config ){}

/**
 * Required for rviz plug-in
 */
void StatsPlugin::onInitialize(){}


// Tell pluginlib about this class.  Every class which should be
// loadable by pluginlib::ClassLoader must have these two lines
// compiled in its .cpp file, outside of any namespace scope.
#include <pluginlib/class_list_macros.h>
PLUGINLIB_DECLARE_CLASS( re2uta_ocuPlugins, Stats, StatsPlugin, rviz::Panel )
